﻿using UnityEngine;
using System.Collections;

public class follow_object : MonoBehaviour {

	public GameObject target_object;
	public Vector3 offset;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
		Vector3 new_position = Vector3.Lerp(transform.position, 
											target_object.transform.position + offset,
											0.1f);

		transform.position = new_position;
	}
}
