﻿using UnityEngine;
using System.Collections;

public class player_controller : MonoBehaviour {

	public float running_speed = 4.0f;
	public GameObject bullet_prefab;
	public float previous_shot_timestamp = 0.0f;
	public float shot_cooldown = 10.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		movement();
		firing_controls();
	}

	void movement()
	{
		Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

		Rigidbody rb = GetComponent<Rigidbody>();
		rb.velocity = new Vector3(0, rb.velocity.y, 0) + new Vector3(input.x, 0, input.y) * running_speed;
		print(input.magnitude.ToString());


		if(input.magnitude > 0.5f)
			transform.forward = new Vector3(input.x, 0, input.y);

		/*
		if(Input.GetKey(KeyCode.RightArrow))
		{
			// Move object right.
			Rigidbody rb = GetComponent<Rigidbody>();
			rb.velocity = new Vector3(running_speed, 0, 0);
			transform.forward = new Vector3(1, 0, 0);
		}

		if(Input.GetKey(KeyCode.LeftArrow))
		{
			// Move object right.
			Rigidbody rb = GetComponent<Rigidbody>();
			rb.velocity = new Vector3(-running_speed, 0, 0);
			transform.forward = new Vector3(-1, 0, 0);

		}

		if(Input.GetKey(KeyCode.UpArrow))
		{
			// Move object right.
			Rigidbody rb = GetComponent<Rigidbody>();
			rb.velocity = new Vector3(0, 0, running_speed);
			transform.forward = new Vector3(0, 0, 1);
		}

		if(Input.GetKey(KeyCode.DownArrow))
		{
			// Move object right.
			Rigidbody rb = GetComponent<Rigidbody>();
			rb.velocity = new Vector3(0, 0, -running_speed);
			transform.forward = new Vector3(0, 0, -1);
		}*/


	}

	void firing_controls()
	{
		// Shooting
		if(Input.GetKey(KeyCode.Space))
		{
			// Consider cooldown.
			if(Time.time - previous_shot_timestamp >= shot_cooldown)
			{
				previous_shot_timestamp = Time.time;

				GameObject new_bullet = Instantiate(bullet_prefab, transform.position,
					Quaternion.identity) as GameObject;

				Rigidbody new_bullet_rb = new_bullet.GetComponent<Rigidbody>();
				new_bullet_rb.velocity = transform.forward * 15 + UnityEngine.Random.onUnitSphere;
			}
		}
	}
}
